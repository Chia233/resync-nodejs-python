import requests

def test_arithmetic_divide():
    #typical usecase 
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=1&y=5&operation=/')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['results']
    assert getResult == 0.2
    
    #x not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&y=5&operation=/')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=1&operation=/')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #wrong x value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=never&y=5&operation=/')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #wrong y value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=1&y=gonna&operation=/')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."


def test_arithmetic_multiply():
    #typical usecase
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=5&y=2&operation=*')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['results']
    assert getResult == 10

    #x not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&y=2&operation=*')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=5&operation=*')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #x wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=give&y=2&operation=*')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=5&y=you&operation=*')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

def test_arithmetic_add():
    #typical usecase
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=10&y=10&operation=+')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['results']
    assert getResult == 20

    #x not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&y=10&operation=+')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=10&operation=+')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #x wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=up&y=10&operation=+')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=10&y=never&operation=+')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

def test_arithmetic_minus():
    #typical usecase
    response = requests.post('http://127.0.0.1:5000/arithmetic?x=10&y=5&operation=-')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['results']
    assert getResult == 5

    #x not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&y=5&operation=-')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y not parsed
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=10&operation=-')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #x wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=gonna&y=5&operation=-')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

    #y wrong value
    response = requests.post('http://127.0.0.1:5000/arithmetic?&x=10&y=let&operation=-')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['status']
    assert getResult == "400 Bad Request: The browser (or proxy) sent a request that this server could not understand."

def test_get_users():
    #typical usecase
    response = requests.get('http://127.0.0.1:5000/users')
    assert response.status_code == 200

def test_get_specific_user():
    #getting geralt
    response = requests.get('http://127.0.0.1:5000/users/geralt')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['id']
    assert getResult == "whitewolf"

    #getting lara croft
    response = requests.get('http://127.0.0.1:5000/users/lara_croft')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['id']
    assert getResult == "m31a3n6sion"

    #getting mario
    response = requests.get('http://127.0.0.1:5000/users/mario')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['id']
    assert getResult == "smb3igiul"

    #never getting HL3
    response = requests.get('http://127.0.0.1:5000/users/gordon_freeman')
    assert response.status_code == 200
    getResult = response.json()
    getResult = getResult['id']
    assert getResult == "nohalflife3"
    